---
tags: wikipedia, community event, ambassadors
---

Develop and enrich Software Heritage's Wikipedia pages
===

#wikipedia

# 👨‍👩‍👦‍👦 Workshop participants 👨‍👩‍👦‍👦

- Océane Valencia
- Cécile Arènes
- David Douard
- Moritz
- Isabelle Astic
- Elisabetta Mori

# 🎯 Objectives 🎯
- Description of the workshop: The aim of this workshop is to contribute to the completion and updating of the Software Heritage Wikipedia pages. The French and English Wikipedia pages on Software Heritage lack information and need to be updated. Many references could be added to improve the quality of the page. Concepts could be developed to present the different aspects of Software Heritage. 
- How could we go further for Software Heritage? The objective of this work is to contribute to a better understanding of Software Heritage. Many people are still unaware that code and software is a natively digital heritage that needs to be preserved and enhanced. Raising awareness of Software Heritage is fundamental to the development of code and software preservation.

# ⏳ Workshop resume ⏳

- Welcomes 5'
- Wikipedia rules 5'
    - Focus on primary and secondary sources
- Discover Software Heritage's Wikipedia pages and find the gaps 10'
    - Optional, French / English subgroups
- Review of gaps 5'
    - Choose one gap ;)
- What additions should be made by gap and where? 25'
	- Below, a secondary sources list to add and tools
    - Optional, add your secondary sources in the list below
    - Add your sources in Wikipedia pages!
- Feedback ; What next? 10'

# Optional: create a Wikipedia account 

- https://en.wikipedia.org/w/index.php?title=Special:CreateAccount&returnto=Wikipedia%3AWhy+create+an+account%3F 

# 🌎 Wikipedia pages 🌎

- [https://fr.wikipedia.org/wiki/Software_Heritage](https://fr.wikipedia.org/wiki/Software_Heritage)
- [https://en.wikipedia.org/wiki/Software_Heritage](https://en.wikipedia.org/wiki/Software_Heritage)

---

# Identified Improvements

- no SWHID page
- [x] missing Julia Lawall as Scientific Advisor
- ENEA mirror is now open
- Software Stories
- PTCC maybe?
- Link to iherent identifiers, for example the page https://en.wikipedia.org/wiki/Persistent_identifier links to it but there is little information on the definition of swhid is strctured, maybe it would be good to follow the example on https://en.wikipedia.org/wiki/Uniform_Resource_Identifier
- SWH deposit (save code now/add forge now) "Software source code is collected by crawling code hosting platforms, like GitHub, GitLab.com or Bitbucket, and package archives, like npm or PyPI, and ingested into a special data structure, a Merkle DAG, that is the core of the archive.[6] Each artifact in the archive is associated with an identifier called a SWHID.[7]
- history: explain why SH works whith Unesco, PNSO
- not easy to understand for all the readers, mainly written for scientists (too much technical)
- add examples with lost code on Bitbucket/gitorious/googlecode
- link with reproducibility for researchers
- work with HAL

## In a nutshell

- updates: SHWID, ENEA, HAL, metrics
- history: 
    - explain the aim of SH, perhaps with examples about lost codes
    - explain why SH is partner with Unesco and so on
- research: make an archive is good for reproducibility
- add more explicital contents about softwares as culturals objects

---

# 📚 Sources 📚

*In English and in French*

- You should give priority to secondary sources
- If you find other secondary sources, please add them

## Secondary sources

- [ ] Red Hat (Director). (2022, December 1). Software Heritage adopts Ceph to preserve software code for future generations. [https://www.youtube.com/watch?v=Gt7-KTZO5pE](https://www.youtube.com/watch?v=Gt7-KTZO5pE)
- [ ] Sandström, M., Abrams, M., Bjaalie, J. G., Hicks, M., Kennedy, D. N., Kumar, A., Poline, J.-B., Roy, P. K., Tiesinga, P., Wachtler, T., & Goscinski, W. J. (2022). Recommendations for repositories and scientific gateways from a neuroscience perspective. Scientific Data, 9(1), Article 1. [https://doi.org/10.1038/s41597-022-01334-1](https://doi.org/10.1038/s41597-022-01334-1)
- [ ] EOSC Executive Board & EOSC Secretariat. (2020). Scholarly infrastructures for research software. Report from the EOSC Executive Board Working Group (WG) Architecture Task Force (TF) SIRS. European Commission. Directorate General for Research and Innovation. [https://data.europa.eu/doi/10.2777/28598](https://data.europa.eu/doi/10.2777/28598)
- [ ] Giry, C., & Barthez, A.-S. (2021). Politique des données, des algorithmes et des codes sources Feuille de route (2021-2024). Ministère de l’Enseignement Supérieur, de la Recherche et de l’Innovation. [https://www.enseignementsup-recherche.gouv.fr/sites/default/files/2021-09/la-feuille-de-route-2021-2024-du-mesri-relative-la-politique-des-donn-es-des-algorithmes-et-des-codes-sources-12965.pdf](https://www.enseignementsup-recherche.gouv.fr/sites/default/files/2021-09/la-feuille-de-route-2021-2024-du-mesri-relative-la-politique-des-donn-es-des-algorithmes-et-des-codes-sources-12965.pdf)
- [ ] Bonamy, C., Boudinet, C., Bourgès, L., Dassas, K., Lefèvre, L., Ninassi, B., & Vivat, F. (2022). Je code: Les bonnes pratiques en éco-conception de service numérique à destination des développeurs de logiciels (pp. 1–19). [https://hal.science/hal-03009741](https://hal.science/hal-03009741)
- [ ] Plan National pour la Science Ouverte 2 (PNSO2) : Axe 3 “Ouvrir et promouvoir les codes sources produits par la recherche” ; le Baromètre de la science ouverte inclut un volet dédié au logiciel
- [ ] CNRS Info. (2020, December 9). Software Heritage: « une infrastructure partagée, dédiée à la recherche, à l’industrie et au patrimoine culturel » | CNRS. CNRS Info. [https://www.cnrs.fr/fr/cnrsinfo/software-heritage-une-infrastructure-partagee-dediee-la-recherche-lindustrie-et-au](https://www.cnrs.fr/fr/cnrsinfo/software-heritage-une-infrastructure-partagee-dediee-la-recherche-lindustrie-et-au)
- [ ] Di Cosmo Roberto. (2021). [Interview] Software Heritage, une archive pour collecter et préserver le code source. Bulletin des bibliothèques de France [https://bbf.enssib.fr/consulter/bbf-2021-00-0000-002](https://bbf.enssib.fr/consulter/bbf-2021-00-0000-002).
- [ ] Le Progrès. (2016) High-tech. La France veut créer une « bibliothèque universelle » du logiciel. [https://www.leprogres.fr/lifestyle/2016/07/03/la-france-veut-creer-une-bibliotheque-universelle-du-logiciel](https://www.leprogres.fr/lifestyle/2016/07/03/la-france-veut-creer-une-bibliotheque-universelle-du-logiciel).
- [x] ~~UNESCO. (2017). Discours de la Directrice générale de l’UNESCO, Irina Bokova, à l’occasion de la signature de l’accord entre l’UNESCO et INRIA portant sur la préservation et le partage du patrimoine logiciel. UNESCO. [https://unesdoc.unesco.org/ark:/48223/pf0000247817](https://unesdoc.unesco.org/ark:/48223/pf0000247817)~~


## Primary sources

- [ ] Gruenpeter, M., Monteil, A., Nivault, E., & Sadowska, J. (2021). _Garantir la cohérence des données constitue le cœur de notre activité: Entretien autour des enjeux descriptifs du code source_. _1_. [https://bbf.enssib.fr/consulter/bbf-2021-00-0000-004](https://bbf.enssib.fr/consulter/bbf-2021-00-0000-004)
- [ ] Di Cosmo, R. (2020). Archiving and Referencing Source Code with Software Heritage. In A. M. Bigatti, J. Carette, J. H. Davenport, M. Joswig, & T. de Wolff (Eds.), _Mathematical Software – ICMS 2020_ (Vol. 12097, pp. 362–373). Springer International Publishing. [https://doi.org/10.1007/978-3-030-52200-1_36](https://doi.org/10.1007/978-3-030-52200-1_36)
- [ ] Di Cosmo, R., Gruenpeter, M., & Zacchiroli, S. (2020). Referencing Source Code Artifacts: A Separate Concern in Software Citation. _Computing in Science & Engineering_, _22_(2), 33–43. [https://doi.org/10.1109/MCSE.2019.2963148](https://doi.org/10.1109/MCSE.2019.2963148)
- [ ] Di Cosmo, R. (2017, April). Software Heritage: Pourquoi et comment construire l’archive universelle du code source. _Bulletin de La Société Informatique de France_, _10_, 67–72.https://www.societe-informatique-de-france.fr/wp-content/uploads/2017/04/1024-no10-Software-Heritage.pdf
- [ ] Di Cosmo, R. (2021). Software Heritage, une archive pour collecter et préserver le code source. _Bulletin des bibliothèques de France_, _1_. [https://bbf.enssib.fr/consulter/bbf-2021-00-0000-00](https://bbf.enssib.fr/consulter/bbf-2021-00-0000-002)
- [ ] Barborini, Y., Cosmo, R. D., Dumont, A. R., Gruenpeter, M., Marmol, B., Monteil, A., Sadowska, J., & Zacchiroli, S. (2018, March 21). _The creation of a new type of scientific deposit: Software_. RDA Eleventh Plenary Meeting, Berlin, Germany. [https://hal.inria.fr/hal-01738741](https://hal.inria.fr/hal-01738741)
- [x] ~~EOSC Executive Board & EOSC Secretariat. (2020). _Scholarly infrastructures for research software. Report from the EOSC Executive Board Working Group (WG) Architecture Task Force (TF) SIRS_. European Commission. Directorate General for Research and Innovation. [https://data.europa.eu/doi/10.2777/28598](https://data.europa.eu/doi/10.2777/28598)~~
- [x] ~~Abramatic, J.-F., Di Cosmo, R., & Zacchiroli, S. (2018). Building the universal archive of source code. _Communications of the ACM_, _61_(10), 29–31. [https://doi.org/10.1145/3183558](https://doi.org/10.1145/3183558)~~
- [x] ~~UNESCO. (2017). _Discours de la Directrice générale de l’UNESCO, Irina Bokova, à l’occasion de la signature de l’accord entre l’UNESCO et INRIA portant sur la préservation et le partage du patrimoine logiciel_. UNESCO. [https://unesdoc.unesco.org/ark:/48223/pf0000247817](https://unesdoc.unesco.org/ark:/48223/pf0000247817)~~

---

# 🛠️ Tools 🛠️

- Software Heritage glossary https://docs.softwareheritage.org/devel/glossary.html

# 📜 Wikipedia rules 📜

- Ten simple rules (see 4, 6, 7, 8): https://en.wikipedia.org/wiki/Wikipedia:Ten_simple_rules_for_editing_Wikipedia
- For researchers: https://en.wikipedia.org/wiki/Help:Wikipedia_editing_for_researchers,_scholars,_and_academics
- Remember: no unpublished information, no new content

---

# 🏁 Feedback ; What next? 🏁

- Add the elements and sources identified on the English page.
- Have the new version proofread by people unfamiliar with Software Heritage and research, to ensure that the page is accessible to all audiences.
    - [name=sabrina] do you already have a specific target in mind or would you need some support to organize this? I have an idea of collaboration, but need to check first with you your needs. 
- Complete the page in French
    - [name=sabrina] we could organize an online translation sprint? Tell me if you need some support for this. I can share with you the methodology used a couple of year ago for the translation sprint for the Foster handbook. 
- Arrange for the page to be translated into Italian, Spanish and German
    - [name=sabrina] same comment as above. For the Spanish version, we have resources in the ambassadors network. (Even in Portuguese)
    - [name=sabrina] please, could we add as a next step a planification activity to design a calendar and check the resources needed? maybe, we could do the draft in an asynchronous way and then, scheduling a call to discuss it. Let me know what is your favourite option. 
THANKS for this wonderful input. :-) 
