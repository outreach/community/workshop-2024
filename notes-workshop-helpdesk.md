# 2024-01-31 Notes taking template/ Helpdesk: Supporting the Software Heritage community

Name of the note taker: Lunar

Issues
------

(*non-exhaustive list of identified issues*)

“As a researcher, it’s hard when looking at the documentation to find the Matrix channel where you can ask questions.”

Matrix is not meant to be a source of truth. We already have FAQ.

The `swh-users` mailing list is underused. About 20 messages a year. People ask on Matrix or they ask Zack or Roberto directly. People write to French on the mailing lists.

There is no places where it says ”you should ask for help here”.

We have no process about who should be answering.

The documentation is not focused enough for our various types of users.

The knowledge base is spread in many places and a lot of information are not written down.

Possible motto
--------------

“Consider every mail to the helpdesk as a bug” (maybe it’s software issue, UX/design issue, or something missing in the documentation) → this approach does not work if the requests are about access management 

Helpdesk perimeter
------------------

Support level 1 only?

When we talk about requests, we consider only requests coming from outside the team.

Kind of requests:

- Creating accounts (eg. on deposit, gitlab)
    - Sometimes we need a process to verify if there are legitimate requests
- Help to scientists who want to use Software Heritage
- …
- (probably there would be requests related to ambassadors or outreach?)

We are likely to receive requests thare are not software or ops related. 

Example of requests :

- What are the difference between “partial” or “full” origin visit status?
- Clarification about the ethical guidelines:
  <https://sympa.inria.fr/sympa/arc/swh-users/2023-11/msg00002.html>
- What could I do to help?
- Problem in cooking:
  <https://sympa.inria.fr/sympa/arc/swh-devel/2024-01/msg00011.html>
- Mentor Software Heritage at CodePeak 2023!
  <https://sympa.inria.fr/sympa/arc/swh-devel/2023-12/msg00001.html>
  (not for the helpdesk?)
- Request for an interview: expert opinion on the Cyber Resilience Act
  <https://sympa.inria.fr/sympa/arc/swh-devel/2023-07/msg00003.html>
  (not for the helpdesk?)
- Rate-limit issues
- Is there any way to do X? If not, is this in your plans to add such a feature?




