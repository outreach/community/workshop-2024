# Community workshop 2024
Event day: Wednesday, January 31st at INRIA research center
Starts at: 10.00
Ends at: 17.00 (at the latest)

## Quick links
- SWH website: https://www.softwareheritage.org/
- SWH archive: https://archive.softwareheritage.org/
- Template for note taking: https://hedgedoc.softwareheritage.org/AZAPo5xoSDG-SUZb9jiDiQ# 
- SWH Code of conduct: https://gitlab.softwareheritage.org/swh/devel/swh-docs/-/blob/master/CODE_OF_CONDUCT.md
- Workshops proposals made by the ambassadors: https://hedgedoc.softwareheritage.org/2024-community-event-workshop-proposals?both

## Workshop details
### What are the goals of the workshop? 
Building the SWH community and sharing knowledge within the community.

### What are the prerequisites to attend? 
Being part of the Software Heritage community.

### Will the work done during the activity be presented to the other attendees? 
Yes, for each activity, the lead will need:
- someone who will take notes with the template (cf. quick links)
- someone who will contribute to the report back in the plenary session

The same person can play these 2 roles. 

### Do I need a laptop? 
You can come without a laptop but a laptop per group will be useful.

## Getting started

### Before the workshop
1. Step 1: Create a SWH Gitlab account 
2. Step 2: Comment this issue https://gitlab.softwareheritage.org/outreach/community/workshop-2023/-/issues/2
3. Step 3: Connect to https://matrix.to/#/#swh:matrix.org


## Contributing
All contributions to this repository are under CC-BY-4

## Contributors
### Workshop organizers: 
- Morane Gruenpeter
- Sabrina Granger
- Lunar
- Benoît Chauvet


### Workshop participants:
Please add yourself to this list as contributors.
- Cécile Arènes (ambassador)
- Agustin Benito Bethencourt (ambassador)
- Elias Chetouane
- Valentin Lorentz (SWH)
- Violaine Louvet (ambassador)
- Pierre Poulain (ambassador)
- Océane Valencia (ambassador)
-


## License
CC-BY-4

