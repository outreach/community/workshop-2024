# 2024 Community event workshops: your proposals

## Practical info about the community event
- Jan. 31st, 2024
- Inria Centre, 2 rue Simone Iff, Paris
- in person event, no hybrid mode

## What? 
- 45' activity
- target audience: the whole Software Heritage community = devs, project-managers, researchers from all academic fields, advocates, archivists and librarians interested in Software Heritage.
- Max. number of participants: 10
- Activities leads are encouraged to put the accent on active work sessions rather than on presentations without interactions ; the audience should be in an active position
- possible topics: use cases, teaching practices, outreaching and more related to Software Heritage.
- about 10 activities in total will be proposed: 3 time slots are booked for the ambassadors

## As a SWH team member, how can you contribute? 
**By Dec. 15th** 

There are 2 main options (and they don't exclude each other):

- **help us to spot people within the community who could lead an activity**. (FYI: the ambassadors already received the proposal to deliver a workshop: https://hedgedoc.softwareheritage.org/2024-community-event-workshop-proposals?both#). This doesn't mean that you have to contribute to the workshop organization (unless, you wish it): Sabrina will get in touch with the community members spotted. 

- **lead an activity**: if you are volunteer for that, you can deliver a workshop, on a topic you choose. Below, you'll find info to provide a 1st overview of your project. 

### Suggestions of community members who could lead an activity

For each contact: 
- First and last name 
- Email address: 
- Who made the suggestion: 
- Suggestion of activity, or specific topic to be offered (optionnal): 


#### Proposal: Miguel Colom 
- "Miguel Colom" <miguel.pixel@gmail.com>; 
- Who made the suggestion: Sabrina
- Suggestion of activity, or specific topic to be offered (optionnal): topic would be teaching reproducibility using SWH

#### Proposal: 

### Proposals of activities led by a SWH team member

#### Proposal 1
- Who made the proposal? Add your name:
- title of your workshop: 
- short description of the workshop (1 paragraph is fine) : 
- objectives related to Software Heritage: how could we go further for Software Heritage? 
- would you need to be in touch with the technical team, to do some checking? Y/N
- any prerequisites for the contributors? 

#### Proposal 2
- Who made the proposal? Add your name:
- title of your workshop: 
- short description of the workshop (1 paragraph is fine) : 
- objectives related to Software Heritage: how could we go further for Software Heritage? 
- would you need to be in touch with the technical team, to do some checking? Y/N
- any prerequisites for the contributors? 

#### Proposal 3
- Who made the proposal? Add your name:
- title of your workshop: 
- short description of the workshop (1 paragraph is fine) : 
- objectives related to Software Heritage: how could we go further for Software Heritage? 
- would you need to be in touch with the technical team, to do some checking? Y/N
- any prerequisites for the contributors? 

etc. ad libitum!
